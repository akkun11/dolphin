基本的には [めいどるふぃんと同じ](https://github.com/mei23/memo/blob/master/misskey/Setup-Dolphin.md) で良さげ。
機能選択のためにC言語のプリプロセッサを使うのでを使うので `cpp` パッケージも入れてください。

# 普通のサーバーで立てる場合

# Dockerで立てる場合

# [Render](https://render.com)で立てる場合

## Redisを立てる

1. New → Redis
2. 設定無変更、インスタンスはFreeで良い。
3. 作成後の設定は適当で良い。
4. `config.yml` の設定は `Redis configuration` のところを `Connect` → `Internal Connection` のホスト名とポートを書く。

## Postgresを立てる

### Renderの場合

無料インスタンスは90日しかデータ保存してくれないので勧めない。

### Vercelの場合

### Supabaseの場合

## オブジェクトストレージを立てる

## Dolphin本体を立てる

1. New → Web Service
2. Build and deploy from a Git repository
3. Public Git repository → `https://gitlab.com/akkun11/dolphin`
4. Branch: `akku` Runtime: `Node` Instance Type: `Free`でギリ動く
   Build Command: `cp /etc/secrets/config.yml .config/default.yml && yarn install && NODE_ENV=production yarn build && yarn migrate`
5. Advanced
   Add Secret File:
    Filename: `config.yml`
    File Contents:編集済 `.config/default.yml`
6. opt:Add Environment Variable
   SupabaseのPostgresを使うときは `NODE_TLS_REJECT_UNAUTHORIZED`:`0` を設定する必要がある。
